import vehicle

car1 = vehicle.Car()
car1.color = "Red"

car2 = vehicle.Car()
car2.color = "White"

vehicle1 = __import__("vehicle")
truck1 = vehicle1.Truck()
truck1.payload = 10

print(car1)
print(truck1)
print('-----------')

from parrot import Parrot

parrot1 = Parrot()
parrot1.name = "Blu"
parrot1.age = 10
print(f"{parrot1.name} is {parrot1.age} years old")
