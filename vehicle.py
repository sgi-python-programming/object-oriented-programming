class Car:
    # class attribute
    wheels = 4
    passengers = 5
    color = None

    def __str__(self):
        return ("Wheels: {}, Passengers: {}, Color: {}"
                .format(self.wheels, self.passengers, self.color))
    # see special method name https://docs.python.org/3/reference/datamodel.html#customization


class Truck:
    wheels = 6
    passengers = 2
    payload = 0

    def __str__(self):
        return (f"Wheels: {self.wheels}, "
                f"Passengers: {self.passengers}, "
                f"Payload: {self.payload}")
